require 'test_helper'

class PensamentosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pensamento = pensamentos(:one)
  end

  test "should get index" do
    get pensamentos_url
    assert_response :success
  end

  test "should get new" do
    get new_pensamento_url
    assert_response :success
  end

  test "should create pensamento" do
    assert_difference('Pensamento.count') do
      post pensamentos_url, params: { pensamento: { description: @pensamento.description, name: @pensamento.name, picture: @pensamento.picture } }
    end

    assert_redirected_to pensamento_url(Pensamento.last)
  end

  test "should show pensamento" do
    get pensamento_url(@pensamento)
    assert_response :success
  end

  test "should get edit" do
    get edit_pensamento_url(@pensamento)
    assert_response :success
  end

  test "should update pensamento" do
    patch pensamento_url(@pensamento), params: { pensamento: { description: @pensamento.description, name: @pensamento.name, picture: @pensamento.picture } }
    assert_redirected_to pensamento_url(@pensamento)
  end

  test "should destroy pensamento" do
    assert_difference('Pensamento.count', -1) do
      delete pensamento_url(@pensamento)
    end

    assert_redirected_to pensamentos_url
  end
end
