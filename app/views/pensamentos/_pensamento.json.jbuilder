json.extract! pensamento, :id, :name, :description, :picture, :created_at, :updated_at
json.url pensamento_url(pensamento, format: :json)
