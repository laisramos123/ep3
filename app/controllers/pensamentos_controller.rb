class PensamentosController < ApplicationController
  before_action :set_pensamento, only: [:show, :edit, :update, :destroy]

  # GET /pensamentos
  # GET /pensamentos.json
  def index
    @pensamentos = Pensamento.all
  end

  # GET /pensamentos/1
  # GET /pensamentos/1.json
  def show
    @comments = @pensamento.comments.all
    @comment = @pensamento.comments.build
  end

  # GET /pensamentos/new
  def new
    @pensamento = Pensamento.new
  end

  # GET /pensamentos/1/edit
  def edit
  end

  # POST /pensamentos
  # POST /pensamentos.json
  def create
    @pensamento = Pensamento.new(pensamento_params)

    respond_to do |format|
      if @pensamento.save
        format.html { redirect_to @pensamento, notice: 'Pensamento was successfully created.' }
        format.json { render :show, status: :created, location: @pensamento }
      else
        format.html { render :new }
        format.json { render json: @pensamento.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pensamentos/1
  # PATCH/PUT /pensamentos/1.json
  def update
    respond_to do |format|
      if @pensamento.update(pensamento_params)
        format.html { redirect_to @pensamento, notice: 'Pensamento was successfully updated.' }
        format.json { render :show, status: :ok, location: @pensamento }
      else
        format.html { render :edit }
        format.json { render json: @pensamento.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pensamentos/1
  # DELETE /pensamentos/1.json
  def destroy
    @pensamento.destroy
    respond_to do |format|
      format.html { redirect_to pensamentos_url, notice: 'Pensamento was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pensamento
      @pensamento = Pensamento.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pensamento_params
      params.require(:pensamento).permit(:name, :description, :picture)
    end
end
