Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  resources :comments
  devise_for :users
  get 'pages/info'
 get 'pages/doadoras'

  resources :pensamentos

root to: redirect('/pensamentos')
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
