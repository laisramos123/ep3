# Breastfeeding Friends

 Aplicação web desenvolvida através da plataforma Ruby on Rails, na qual tem por objetivo dar voz às problemáticas diárias de mães que amamentam ou não, e proporcionar que pessoas possam encontrar mãe de leite para seus filhos.

Funcionalidades:

* Adição de posts acerca do assunto

* Adição de comentários nos posts

*  Cadastro de perfil com o Gravatar
